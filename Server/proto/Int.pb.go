// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.12.3
// source: Int.proto

package proto 

import (
	context "context"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type IntGeneratorReq struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Length int32 `protobuf:"varint,1,opt,name=length,proto3" json:"length,omitempty"`
}

func (x *IntGeneratorReq) Reset() {
	*x = IntGeneratorReq{}
	if protoimpl.UnsafeEnabled {
		mi := &file_Int_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IntGeneratorReq) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IntGeneratorReq) ProtoMessage() {}

func (x *IntGeneratorReq) ProtoReflect() protoreflect.Message {
	mi := &file_Int_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*IntGeneratorReq) Descriptor() ([]byte, []int) {
	return file_Int_proto_rawDescGZIP(), []int{0}
}

func (x *IntGeneratorReq) GetLength() int32 {
	if x != nil {
		return x.Length
	}
	return 0
}

type IntGeneratorRes struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Int string `protobuf:"bytes,1,opt,name=int,proto3" json:"int,omitempty"`
}

func (x *IntGeneratorRes) Reset() {
	*x = IntGeneratorRes{}
	if protoimpl.UnsafeEnabled {
		mi := &file_Int_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *IntGeneratorRes) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*IntGeneratorRes) ProtoMessage() {}

func (x *IntGeneratorRes) ProtoReflect() protoreflect.Message {
	mi := &file_Int_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

func (*IntGeneratorRes) Descriptor() ([]byte, []int) {
	return file_Int_proto_rawDescGZIP(), []int{1}
}

func (x *IntGeneratorRes) GetInt() string {
	if x != nil {
		return x.Int
	}
	return ""
}

var File_Int_proto protoreflect.FileDescriptor

var file_Int_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x05, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x2e, 0x0a, 0x14, 0x50, 0x61, 0x73, 0x73, 0x77,
	0x6f, 0x72, 0x64, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x52, 0x65, 0x71, 0x12,
	0x16, 0x0a, 0x06, 0x6c, 0x65, 0x6e, 0x67, 0x74, 0x68, 0x18, 0x01, 0x20, 0x01, 0x28, 0x05, 0x52,
	0x06, 0x6c, 0x65, 0x6e, 0x67, 0x74, 0x68, 0x22, 0x32, 0x0a, 0x14, 0x50, 0x61, 0x73, 0x73, 0x77,
	0x6f, 0x72, 0x64, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x52, 0x65, 0x73, 0x12,
	0x1a, 0x0a, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x70, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x32, 0x5b, 0x0a, 0x11, 0x50,
	0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x6f, 0x72,
	0x12, 0x46, 0x0a, 0x08, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61, 0x74, 0x65, 0x12, 0x1b, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x50, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x47, 0x65, 0x6e,
	0x65, 0x72, 0x61, 0x74, 0x6f, 0x72, 0x52, 0x65, 0x71, 0x1a, 0x1b, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x50, 0x61, 0x73, 0x73, 0x77, 0x6f, 0x72, 0x64, 0x47, 0x65, 0x6e, 0x65, 0x72, 0x61,
	0x74, 0x6f, 0x72, 0x52, 0x65, 0x73, 0x22, 0x00, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_Int_proto_rawDescOnce sync.Once
	file_Int_proto_rawDescData = file_Int_proto_rawDesc
)

func file_Int_proto_rawDescGZIP() []byte {
	file_Int_proto_rawDescOnce.Do(func() {
		file_Int_proto_rawDescData = protoimpl.X.CompressGZIP(file_Int_proto_rawDescData)
	})
	return file_Int_proto_rawDescData
}

var file_Int_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_Int_proto_goTypes = []interface{}{
	(*IntGeneratorReq)(nil), // 0: proto.IntGeneratorReq
	(*IntGeneratorRes)(nil), // 1: proto.IntGeneratorRes
}
var file_Int_proto_depIdxs = []int32{
	0, // 0: proto.IntGenerator.Generate:input_type -> proto.IntGeneratorReq
	1, // 1: proto.IntGenerator.Generate:output_type -> proto.IntGeneratorRes
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_Int_proto_init() }
func file_Int_proto_init() {
	if File_Int_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_Int_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IntGeneratorReq); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_Int_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*IntGeneratorRes); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_Int_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_Int_proto_goTypes,
		DependencyIndexes: file_Int_proto_depIdxs,
		MessageInfos:      file_Int_proto_msgTypes,
	}.Build()
	File_Int_proto = out.File
	file_Int_proto_rawDesc = nil
	file_Int_proto_goTypes = nil
	file_Int_proto_depIdxs = nil
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConnInterface

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion6

// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type IntGeneratorClient interface {
	Generate(ctx context.Context, in *IntGeneratorReq, opts ...grpc.CallOption) (*IntGeneratorRes, error)
}

type intGeneratorClient struct {
	cc grpc.ClientConnInterface
}

func NewIntGeneratorClient(cc grpc.ClientConnInterface) IntGeneratorClient {
	return &intGeneratorClient{cc}
}

func (c *intGeneratorClient) Generate(ctx context.Context, in *IntGeneratorReq, opts ...grpc.CallOption) (*IntGeneratorRes, error) {
	out := new(IntGeneratorRes)
	err := c.cc.Invoke(ctx, "/proto.IntGenerator/Generate", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// IntGeneratorServer is the server API for IntGenerator service.
type IntGeneratorServer interface {
	Generate(context.Context, *IntGeneratorReq) (*IntGeneratorRes, error)
}

// UnimplementedIntGeneratorServer can be embedded to have forward compatible implementations.
type UnimplementedIntGeneratorServer struct {
}

func (*UnimplementedIntGeneratorServer) Generate(context.Context, *IntGeneratorReq) (*IntGeneratorRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Generate not implemented")
}

func RegisterIntGeneratorServer(s *grpc.Server, srv IntGeneratorServer) {
	s.RegisterService(&_IntGenerator_serviceDesc, srv)
}

func _IntGenerator_Generate_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IntGeneratorReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(IntGeneratorServer).Generate(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.IntGenerator/Generate",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(IntGeneratorServer).Generate(ctx, req.(*IntGeneratorReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _IntGenerator_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.IntGenerator",
	HandlerType: (*IntGeneratorServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Generate",
			Handler:    _IntGenerator_Generate_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "int.proto",
}
