package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
        
    "gopkg.in/ini.v1"

	"gitlab.com/R1mty/Int/Server/proto"
)

type Server struct {

}

func (s *Server) Generate(ctx context.Context, req *proto.IntGeneratorReq) (*proto.IntGeneratorRes, error) {
	if req.GetLength() < 0 {

		return nil, fmt.Errorf("введено отрицательное число!")
	}
	int := make([]byte, req.GetLength())

	for i := int32(0); i < req.Length; i++ {
		int[i] = byte('0' + rand.Int31n(10))
	}

	log.Printf("Число: %s\n", int)

	return &proto.IntGeneratorRes{Int: string(int)}, nil
}

func main() {
	host := ""
	port := ""
	_, err := os.Stat("intLabServer.ini")
	// if err == nil {
	// 	li("Config file intLab.ini found")
	// 	cfg, err := ini.Load("intLab.ini")
	// 	if err != nil {
	// 		fmt.Printf("Fail to read file: %v", err)
	// 	} else {
	// 		// Default parameters
	// 		host = cfg.Section("intLab").Key("host").String()
	// 		port = cfg.Section("intLab").Key("port").String()
	// 	}
	// }
	cfg, err := ini.Load("intLabServer.ini")
	host = cfg.Section("intLab").Key("host").String()
	port = cfg.Section("intLab").Key("port").String()
	if host == "" {
		host = "127.0.0.1"
	}
	if port == "" {
		port = "9090"
	}
	listener, err := net.Listen("tcp", fmt.Sprintf(":%v", port))
	if err != nil {
		log.Fatalf("Failed to listen gRPC on port %v: %v", port, err)
	}

	server := grpc.NewServer(grpc.KeepaliveParams(keepalive.ServerParameters{}))

	proto.RegisterIntGeneratorServer(server, &Server{})

	if err := server.Serve(listener); err != nil {
		log.Fatalf("Failed to listen on port %v: %v", port, err)
	}
}
